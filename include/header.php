<nav class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark">
  <a class="navbar-brand" href=""><img src="img/favicon.png" style="width:30px"></img></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard">NEW RECORD</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="list">LIST</a>
      </li>
    </ul>
    <ul class="navbar-nav pull-right">
      <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php echo $_SESSION['user_loggedcode000'];?>
      </a>
      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
        <a class="dropdown-item" href="data/logout.php">Logout</a>
      </div>
      </li>
    </ul>
  </div>
</nav>

<?php
  if($_SESSION['alert'] === "success"){?>
    <div id="success_alert" class="alert alert-warning alert-dismissible fade show" role="alert" ><!-- success notif-->
      <strong>SUCCESS!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
<?php
  $_SESSION['alert'] = "";
  } else if($_SESSION['alert'] === "error"){?>
    <div id="error_alert" class="alert alert-warning alert-dismissible fade show" role="alert"><!-- success notif-->
      <strong>ERROR!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
<?php
  $_SESSION['alert'] = "";
  } ?>

<?php
include_once('connection.php');

class database extends dbc{

  public function __construct(){
    parent:: __construct();//get construct of class_parENTS

    if(session_status() == PHP_SESSION_NONE){
      session_start();
    }
  }

  public function getRow($query,$type,$params = []){

    $stmt = $this->conn->prepare($query);
    $stmt->bind_param($type,...$params);
    $stmt->execute();
    $get_res = $stmt->get_result();
    if(mysqli_num_rows($get_res) > 0){
      while ($rows = $get_res->fetch_assoc()) {
            $data[] = $rows;
      }
    }else {
      $data = array();
    }
    return $data;
    $stmt->close();
  }

  public function getallRow($query){

    $stmt = $this->conn->prepare($query);
    $stmt->execute();
    $get_res = $stmt->get_result();
    if(mysqli_num_rows($get_res) > 0){
      while ($rows = $get_res->fetch_assoc()) {
            $data[] = $rows;
      }
    }else {
      $data = array();
    }
    return $data;
    $stmt->close();
  }

  public function insertRow($query,$type,$params = []){

    $stmt = $this->conn->prepare($query);
    $stmt->bind_param($type,...$params);
    if($stmt->execute()){
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  public function updateRow($query,$type,$params = []){

    $stmt = $this->conn->prepare($query);
    $stmt->bind_param($type,...$params);
    if($stmt->execute()){
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }

  public function deleteRow($query,$type,$params = []){

    $stmt = $this->conn->prepare($query);
    $stmt->bind_param($type,...$params);
    if($stmt->execute()){
      return true;
    }else {
      return false;
    }
    $stmt->close();
  }
}//class
 ?>

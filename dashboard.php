<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
<form method="post" action="data/add_record.php" enctype="multipart/form-data">
  <div class="container-fluid">
    <?php include('include/alerts.php'); ?>
    <div class="row">
      <div class="col-sm-6">
        <div class="card border-dark" style="height:100%; margin-bottom:25px">
          <div class="card-header text-center h5">Personal Information</div>
          <div class="card-body">
            <div class="form-group row">
              <div class="col-sm-12">
                <div class="text-center">
                  <img id="output"  src="img/profileimg.jpg"class="img-thumbnail" alt="Your Image" style="width:240px; height:250px;"><br/>
                  <input type="file" title="" id="output1" name="output1" accept="image/*" onchange="loadFile(event)"/>
                </div>
              </div>
            </div>
            <hr>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">First Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="first" id="first" placeholder="Enter First Name" required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Middle Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="middle" name="middle" placeholder="Enter Middle Name" required>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-sm-3 col-form-label">Last Name</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="last" name="last" placeholder="Enter Last Name" required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Gender</label>
                <div class="col-sm-9">
                  <select id="gender" name="gender" class="form-control" name="gender"  required>
                    <option value="" disabled selected >Select gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label input-lg">Birth Date</label>
              <div class="col-sm-4">
                <select id="month" name="month" class="form-control input-lg"  required>
                  <option value="" disabled selected >Month</option>
                  <option value="January">January</option>
                  <option value="February">February</option>
                  <option value="March">March</option>
                  <option value="April">April</option>
                  <option value="May">May</option>
                  <option value="August">August</option>
                  <option value="September">September</option>
                  <option value="October">October</option>
                  <option value="November">November</option>
                  <option value="December">December</option>
                </select>
              </div>
              <div class="col-sm-2">
                <?php
                  echo ("<select id='day' name='day' class='form-control input-lg' required>");
                    echo ("<option value='' disabled selected>Day</option>");
                          for($a=1;$a<=31;$a++){
                            echo ("<option value='".$a."'>".$a."</option>");
                           }echo "</select>";?>
              </div>
              <div class="col-sm-3">
                <?php
                  echo ("<select id='year' name='year' class='form-control input-lg'  required>");
                    echo ("<option value='' disabled selected>Year</option>");
                      for($a=2017;$a>=1950;$a--){
                        echo ("<option value='".$a."'>".$a."</option>");
                       }echo ("</select>"); ?>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label input-lg">Contact Number</label>
                <div class="col-sm-9">
                  <input type="number" step="000000000000" class="form-control input-lg" id="contact" name="contact" placeholder="Enter Contact No." required>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card border-dark" style="height:100%; margin-bottom:25px">
          <div class="card-header text-center h5">Employment Status & Others</div>
          <div class="card-body">
            <div class="form-group row">
              <label class="col-sm-3 col-form-label  input-lg">Section</label>
                <div class="col-sm-9">
                  <select id="section" name="section" class="form-control" required>
                    <option value=""></option>
                    <option value="Planning">Planning Dept.</option>
                    <option value="Warehouse">Warehouse Dept.</option>
                    <option value="Purchasing">Purchasing Dept.</option>
                    <option value="Production">Production Dept.</option>
                    <option value="Accounting">Accounting Dept.</option>
                    <option value="Human resources">Human Resources Dept.</option>
                    <option value="Engineering">Engineering Dept.</option>
                    <option value="Customer service">Customer Service Dept.</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">I.D Number</label>
              <div class="col-sm-9">
                <input type="number" class="form-control" id="idnum" name="idnum" placeholder="Enter ID Number" required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label ">Position</label>
              <div class="col-sm-9">
                <input type="text" class="form-control " id="position" name="position" placeholder="Enter Position" required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label ">Employment Status</label>
              <div class="col-sm-9">
                <select id="status" name="status" class="form-control  input-lg"required>
                  <option value="" disabled selected></option>
                  <option value="contractual">Contractual</option>
                  <option value="probationary">Probationary</option>
                  <option value="regular">Regular</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label input-lg">Date Hired</label>
              <div class="col-sm-9">
                 <input  type="text"  class="form-control input-lg" name="date_hired" id="date_hired" required>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-9 offset-md-3">
                <input type="text" class="form-control input-lg" id="endcontract" name="endcontract" placeholder="End of Contract If Probationary or Contractual" required readonly>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label input-lg">TIN NO.</label>
              <div class="col-sm-9">
                <input type="text" class="form-control input-lg" id="tin" name="tin" placeholder="Enter TIN No." required>
              </div>
            </div>    
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">SSS NO.</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="sss" name="sss"placeholder="Enter SSS No." required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">PHILHEALTH</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="phil" name="phil" placeholder="Enter Philhealth No." required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">PAG IBIG NO.</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="pagibig" name="pagibig" placeholder="Enter Pagibig No." required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Salary</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="salary" name="salary" placeholder="Enter Salary" required>
              </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label">Remarks</label>
                <div class="col-sm-9">
                  <select id="remarks" name="remarks" class="form-control" name="remarks" style="width:200px;" required>
                    <option value=""></option>
                    <option value="active">Active</option>
                    <option value="notactive">Not Active</option>
                    <option value="others">Others</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
              <label  class="col-sm-3 col-form-label input-lg"></label>
              <div class="col-sm-9">
                  <input type="text" class="form-control input-lg" id="others" name="others" placeholder="Remarks : (if OTHERS)" readonly>
              </div>
            </div>
            <button style="float:right;" type="submit" class="btn btn-primary">Submit Record</button>   
          </div><!-- card body -->
        </div>
      </div> <!-- col sm -->
    </div><!-- orw -->
    
    
  
    

    
    
    </div>
  </div>
</form>
<?php include('include/foot.html'); ?>
<script type="text/javascript" src="assets/js_function.js"></script>
</body>
</html>

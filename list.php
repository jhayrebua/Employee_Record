<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php'); ?>
  <div class="container">
    <?php include('include/alerts.php');?>
    <a tabindex="0" class="btn btn-danger" role="button" data-toggle="popover" data-trigger="focus" title="Instruction">Click me!</a>
    <button type="button" name="button" style="float:right;" class="btn btn-secondary" onclick="location.href='export/excel_export.php'"><span class="fas fa-file-excel"></span> Export as excel</button>
    <hr>
    <div id="employee_list">
    </div>
    <hr>
  </div>
<?php include('include/foot.html'); ?>
<script type="text/javascript" src="assets/js_function.js"></script>
</body>
</html>

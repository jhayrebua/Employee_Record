<?php
//export.php
require_once('../class/c_employee.php');
$output = '';
$result = $employee->listEmployee();

  $output .= '
   <table border="1" cellpadding="5">
      <tr >
        <th style="background-color:gray">Name</th>
        <th style="background-color:gray">Section</th>
        <th style="background-color:gray">Id number</th>
        <th style="background-color:gray">Status</th>
        <th style="background-color:gray">Gender</th>
	      <th style="background-color:gray">Birthday</th>
		    <th style="background-color:gray">Contact</th>
		    <th style="background-color:gray">Remarks</th>
      </tr>
  ';
  foreach($result as $row)
  {
   $output .= '
    <tr>
      <td>'.$row["last"]." ".$row["first"]." ".$row["middle"]. '</td>
      <td>'.$row["section"].'</td>
      <td>'.$row["idnum"].'</td>
      <td>'.$row["status"].'</td>
      <td>'.$row["gender"].'</td>
	    <td>'.$row["birthday"].'</td>
	    <td>'.$row["contact"].'</td>
		  <td>'.$row["remarks"].'</td>
    </tr>
   ';
  }
  $output .= '</table>';
  $date = date('M-d-Y');

  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Record_list('.$date.').xls ');
  echo $output;




?>

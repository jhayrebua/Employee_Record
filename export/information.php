
<?php
 ob_start();
require_once('../class/c_employee.php');

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('../assets/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('exelpack');
$pdf->SetTitle('Print');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData('logo.jpg', '50', "".' ', "");

// set header and footer fonts
//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);  edited**
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, "2");

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->SetMargins(6,5,5);
// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 12,'','');

// add a page
$pdf->AddPage('p', 'A4');
//edited ** $pdf->Write(0, ' ', '', 0, 'S', true, 0, false, false, 0);

$pdf->SetFont('helvetica', '', 8);

// -----------------------------------------------------------------------------
$tbl = <<<EOD
EOD;

// -----------------------------------------------------------------------------
if(isset($_GET['id'])){
//table header
  $id = $_GET['id'];
  $result = $employee->employeeInfo($id);

  if(count($result) > 0 ){
    foreach($result as $row){
      $name = $row['first']." ".$row['middle']." ".$row['last'];
      $rand = rand(1,2322);
      $tbl = '
      <h1 align="center">EMPLOYEE INFORMATION</h1>
      <hr>
      <h1></h1>
      <table width="100%" border="1" cellpadding="11">
        <tr>
          <td width="30%" rowspan="7"><img src="../idimg/'.$row['image'].'" style="width:210px; height:240px;"></td>
          <td width="20%" align="center" style="background-color:gray">FIRST: </td>
          <td width="50%" align="center" >'.$row['first'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">MIDDLE: </td>
          <td align="center">'.$row['middle'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">LAST: </td>
          <td align="center">'.$row['last'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">GENDER: </td>
          <td align="center">'.$row['gender'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">BIRTHDAY: </td>
          <td align="center">'.$row['birthday'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">CONTACT: </td>
          <td align="center">'.$row['contact'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">ID: </td>
          <td align="center">'.$row['idnum'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">STATUS:</td>
          <td align="center" colspan="2">'.$row['status'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">SECTION:</td>
          <td align="center" colspan="2">'.$row['section'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">POSITION:</td>
          <td align="center" colspan="2">'.$row['position'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">DATE HIRED:</td>
          <td align="center" colspan="2">'.$row['date_hired'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">END OF CONTRACT:</td>
          <td align="center" colspan="2">'.$row['endcontract'].'</td>
        </tr>
        <tr>
            <td align="center" style="background-color:gray">TIN:</td>
          <td align="center" colspan="2">'.$row['tin'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">SSS:</td>
          <td align="center" colspan="2">'.$row['sss'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">PHILHEALTH:</td>
          <td align="center" colspan="2">'.$row['philhealth'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">PAGIBIG:</td>
          <td align="center" colspan="2">'.$row['pagibig'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">SALARY:</td>
          <td align="center" colspan="2">'.$row['salary'].'</td>
        </tr>
        <tr>
          <td align="center" style="background-color:gray">REMARKS:</td>
          <td align="center" colspan="2">'.$row['remarks'].'</td>
        </tr>
      </table>
      <h1></h1>
      <hr>
      ';
      }
  }
}

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------
$pdf->lastPage();

//Close and output PDF document
$pdf->Output("$name.pdf'", 'I');

//============================================================+
// END OF FILE
//============================================================+

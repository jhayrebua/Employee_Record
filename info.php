<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php');?>
  <form method="post" action="data/edit_record.php" enctype="multipart/form-data">
    <div class="container-fluid">
       <div class="row">
         <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-dark sidebar">
           <ul class="nav nav-pills flex-column">
             <li class="nav-item">
               <a class="nav-link active" href="info?id=<?php echo $_GET['id'];?>">General Infomartion <span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="training?id=<?php echo $_GET['id'];?>">Training</a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="disciplinary?id=<?php echo $_GET['id'];?>">Disciplinary Action</a>
             </li>
           </ul>
         </nav>
         <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
             <button type="button" name="button" class="btn btn-danger" data-toggle="modal" data-target="#confirm_delete"><span class="fas fa-trash"></span> Delete record</button>
             <a target="_blank" class="btn btn-secondary" href='export/information?id=<?php echo $_GET['id'];?>'><span class="fas fa-print"></span> Print record</a>
           <hr>
           <div id="info">
           </div>
         </main>
       </div>
     </div>
     <?php include('modal/confirm.html'); ?>
    <input type="hidden" name="" id="info_id" value="<?php echo $_GET['id']; ?>">
  </form>
<?php include('include/foot.html'); ?>
<script type="text/javascript" src="assets/js_function.js"></script>
</body>
</html>

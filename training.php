<!DOCTYPE html>
<html lang="en">
<?php include('include/session.php');?>
<?php include('include/head.html'); ?>
<body>
<?php include('include/header.php');?>
  <form method="post" action="data/edit_record.php" enctype="multipart/form-data">
    <div class="container">
       <div class="row">
         <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-dark sidebar">
           <ul class="nav nav-pills flex-column">
             <li class="nav-item">
               <a class="nav-link" href="info?id=<?php echo $_GET['id'];?>">General Infomartion </a>
             </li>
             <li class="nav-item">
               <a class="nav-link active" href="training?id=<?php echo $_GET['id'];?>">Training <span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item">
               <a class="nav-link" href="disciplinary?id=<?php echo $_GET['id'];?>">Disciplinary Action</a>
             </li>
           </ul>
         </nav>
         <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
           <?php include('include/alerts.php');?>
           <button type="button" name="button" class="btn btn-primary" data-toggle="modal" data-target="#training_add"><span class="fas fa-plus"></span> Add Training Record</button>
           <hr>
           <div id="training">
           </div>
         </main>
       </div>
     </div>
    <input type="hidden" name="" id="info_id" value="<?php echo $_GET['id']; ?>">
  </form>
<?php include('include/foot.html'); ?>
<?php include('modal/training.html'); ?>
<script type="text/javascript" src="assets/js_function.js"></script>
</body>
</html>

<?php
require_once('../database/database.php');

class Disciplinary extends database{
  public function listAction($id){
    $query = "SELECT * from hr_offense WHERE id = ?";
    $type = 'i';
    return $this->getRow($query,$type,[$id]);
  }

  public function addAction($id,$offense,$date){
    $query = "INSERT INTO hr_offense(id,offense,year) values(?,?,?)";
    $type = "iss";
    return $this->insertRow($query,$type,[$id,$offense,$date]);
  }

  public function deleteAction($id){
    $query = "DELETE FROM hr_offense WHERE uniq = ?";
    $type = "i";
    return $this->deleteRow($query,$type,[$id]);
  }

  public function deleteAllAction($id){
    $query = "DELETE FROM hr_offense WHERE id = ?";
    $type = "i";
    return $this->deleteRow($query,$type,[$id]);
  }
}

$disciplinary = new Disciplinary();

 ?>

<?php
require_once('../database/database.php');

class Employee extends database{
  public function addEmployee($first,$middle,$last,$section,$idnum,$position,$status,$date_hired,$endcontract,$gender,$birthday,$contact,$tin,$sss,$philhealth,$pagibig,$salary,$remarks,$others,$image){
    $query = "INSERT IGNORE INTO table_hr(first,middle,last,section,idnum,position,status,date_hired,endcontract,gender,birthday,contact
    ,tin,sss,philhealth,pagibig,salary,remarks,others,image) VALUES
    (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $type = "ssssssssssssssssssss";
    return $this->insertRow($query,$type,[$first,$middle,$last,$section,$idnum,$position,$status,$date_hired,$endcontract,$gender,$birthday,$contact,$tin,$sss,$philhealth,$pagibig,$salary,$remarks,$others,$image]);
  }

  public function listEmployee(){
    $query = "SELECT * FROM table_hr";
    return $this->getallRow($query);
  }

  public function employeeInfo($id){
    $query = "SELECT * FROM table_hr WHERE unique_id = ?";
    $type = "i";
    return $this->getRow($query,$type,[$id]);
  }

  public function editInfo($first,$middle,$last,$section,$idnum,$position,$status,$date_hired,$endcontract,$gender,$birthday,$contact,$tin,$sss,$philhealth,$pagibig,$salary,$remarks,$others,$image,$id){
    $query = "UPDATE table_hr SET first = ? ,middle = ? ,last = ? ,section = ? ,idnum = ? ,position = ? ,status = ? ,date_hired = ? ,
    endcontract = ? ,gender = ? ,birthday = ? ,contact = ? ,tin = ? ,sss = ? ,philhealth = ? ,pagibig = ? ,salary = ? ,remarks = ? ,others = ? ,image = ?  WHERE unique_id = ?";
    $type = "ssssssssssssssssssssi";
    return $this->updateRow($query,$type,[$first,$middle,$last,$section,$idnum,$position,$status,$date_hired,$endcontract,$gender,$birthday,$contact,$tin,$sss,$philhealth,$pagibig,$salary,$remarks,$others,$image,$id]);

  }

  public function deleteInfo($id){
    $query = "DELETE FROM table_hr WHERE unique_id = ?";
    $type = "i";
    return $this->deleteRow($query,$type,[$id]);
  }

}

$employee = new Employee();

 ?>

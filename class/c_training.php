<?php
require_once('../database/database.php');

class Training extends database{
  public function listTraining($id){
    $query = "SELECT * from hr_training WHERE id = ?";
    $type = 'i';
    return $this->getRow($query,$type,[$id]);
  }

  public function addTraining($id,$training,$date){
    $query = "INSERT INTO hr_training(id,training,date_attended) values(?,?,?)";
    $type = "iss";
    return $this->insertRow($query,$type,[$id,$training,$date]);
  }

  public function deleteTraining($id){
    $query = "DELETE FROM hr_training WHERE uniq = ?";
    $type = "i";
    return $this->deleteRow($query,$type,[$id]);
  }

  public function deleteAllTraining($id){
    $query = "DELETE FROM hr_training WHERE id = ?";
    $type = "i";
    return $this->deleteRow($query,$type,[$id]);
  }
}

$training = new Training();

 ?>

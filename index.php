<!DOCTYPE html>
<html lang="en">
<?php include('include/head.html');
session_start();
$_SESSION['alert'] = "";
?>
<body>
  <div class="container">
    <br/>
      <div class="card border-dark mb-3" style="width:40%; margin:0 auto">
          <h3 class="card-header text-center">Human Resources</h3>
        <div class="card-body">
          <form method="post" id="login">
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-3">Username: </label>
              <div class="col-sm-9">
                <input type="text" id="un" class="form-control" required autocomplete="off">
              </div>
            </div>
            <div class="form-group row">
              <label for="" class="col-form-label col-sm-3">Password: </label>
              <div class="col-sm-9">
                <input type="password" id="pw" class="form-control" required autocomplete="off">
              </div>
            </div>
              <input type="submit" name="" value="Log In" class="float-right btn btn-primary">
            </div>
          </form>
        </div>
      </div>
  </div>
<?php include('include/foot.html'); ?>
<script type="text/javascript" src="assets/js_function.js"></script>
</body>
</html>

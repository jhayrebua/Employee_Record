var loadFile = function(event) {
  var output = document.getElementById('output');
  output.src = URL.createObjectURL(event.target.files[0]);
};

$( function(){
  $( "#date_hired" ).datepicker({ dateFormat: 'yy-mm-dd'});
  $( "#birth_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
  $( "#year_commit" ).datepicker({ dateFormat: 'yy-mm-dd' });
  $( "#offense_date" ).datepicker({ dateFormat: 'yy-mm-dd' });
});

$(function () {
  $('[data-toggle="popover"]').popover({
    container: 'body',
    content:'To edit/delete information. Click the image of the employee.'
  });
});
$(document).ready(function(){

  $("#login").submit(function(event){ ///E1 //FOR Login
    event.preventDefault();
    var un = $("#un").val();
    var pw = $("#pw").val();

    $.ajax({
      type:'post',
        url:'data/login.php',
          dataType:'json',
            data:{
              un:un,
              pw:pw
            },
            success: function(data){
              console.log(data);
              if(data.logged == 'true'){
                location.href = data.url;
              }else {
                $.notify(data.msg);
              }
            },
            error: function(data){
              alert("ERROR:E1");
            }
    });
  });//SUBMIT

  $("#save_training").click(function(event){ //E2
    event.preventDefault();
    var id = $("#info_id").val();
    var training = $("#training_name").val();
    var date = $("#training_date_attended").val();

    if ((training !="" ) & (date != "")) {
      $.ajax({
        type:'post',
          url:'data/add_training.php',
            data:{
              training:training,
              date:date,
              id:id
            },
            success: function(data){
              console.log(data);
              location.reload();
            },
            error: function(data){
              alert("ERROR:E2");
            }
      });
    }else {
      $.notify("Fill all fields",{z_index:9999});
    }

  });

  $("#save_offense").click(function(event){//e3
    event.preventDefault();
    var id = $("#info_id").val();
    var offense = $("#offense_name").val();
    var date = $("#offense_date").val();

    if ((offense !="" ) & (date != "")) {
      $.ajax({
        type:'post',
          url:'data/add_offense.php',
            data:{
              offense:offense,
              date:date,
              id:id
            },
            success: function(data){
              console.log(data);
              location.reload();
            },
            error: function(data){
              alert("ERROR:E3");
            }
      });
    }else {
      $.notify("Fill all fields",{z_index:9999});
    }
  });


  $("#delete_info").click(function(event){ //E4
    event.preventDefault();

    var id = $("#info_id").val();
    $.ajax({
      type:'post',
        url:'data/delete_info.php',
          data:{
            id:id
          },
          success:function(data){
            console.log(data);
            location.href='list';
          },
          error: function(data){
            alert("ERROR:E4");
          }
    });
  });


  $("#remarks").change(function (){
    var remarks = $("#remarks").val();
    if(remarks=="others"){
    $("#others").attr("readonly", false);
    $("#others").focus();
    }else
      $("#others").attr("readonly", true);
  });

  $("#status").change(function (){
    var status = $("#status").val();
	   $( "#endcontract" ).val("");
	});//remarks change


	$("#date_hired").change(function (){
	 var val  = $("#date_hired").val();

	 var splitter = val.split('-');
   var year = splitter[0];
   var month = splitter[1];
   var day = splitter[2];
	 var counter = 0;
   year = parseInt(year);
   var status = $("#status").val();
   if(status=="contractual"){
		month = parseInt(month) + 5;
		if(month>12){
			while(month>12){
        month--;
				counter++;
      }
			 var year2 = year + 1;
			 var month1 = counter;
       $( "#endcontract" ).val(year2+"-"+month1+"-"+day);
     }else
			 $( "#endcontract" ).val(year+"-"+month+"-"+day);
   }else if(status=="probationary"){
	    month = parseInt(month) + 6;
      if(month>12){
    		while(month>12){
          month--;
    			counter++;
        }
    		var year2 = year + 1;
    		var month1 = counter;
        $( "#endcontract" ).val(year2+"-"+month1+"-"+day);
     }else
    			$( "#endcontract" ).val(year+"-"+month+"-"+day);
    }else
			$( "#endcontract" ).val("N/A");
    }); //date hired


    //F1 (function 1)
    function employee_list(){
      $.ajax({
        type:'post',
          url:'data/employee_list.php',
            success: function(data){
              // console.log(data);
              $("#employee_list").html(data);
            },
            error: function(data){
              alert("ERROR: F1");
            }
      });
    }
    employee_list();

    //F2 (function2)
    function info(){
      var id = $("#info_id").val();
      $.ajax({
        type:'post',
          url:'data/info.php',
            data:{
              id:id
            },
            success: function(data){
              // console.log(data);
              $("#info").html(data);
            },
            error: function(data){
              alert("ERROR: F2");
            }
      });
    }
    info();

    function training(){//F3
      var id = $("#info_id").val();
      $.ajax({
        type: 'post',
          url: 'data/training.php',
            data:{
              id:id
            },
            success: function(data){
              // console.log(data);
              $("#training").html(data);
            },
            error: function(data){
              alert("ERROR F3");
            }
      });
    }
    training();

    function disciplinary(){//F4
      var id = $("#info_id").val();
      $.ajax({
        type: 'post',
          url: 'data/disciplinary.php',
            data:{
              id:id
            },
            success: function(data){
              // console.log(data);
              $("#disciplinary").html(data);
            },
            error: function(data){
              alert("ERROR F4");
            }
      });
    }
    disciplinary();



});//END DOCU READY

<?php
require_once('../class/c_training.php');

if(isset($_GET['id'])){
  $id = $_GET['id'];
  $user_id = $_GET['userid'];

  $result = $training->deleteTraining($id);

  if($result === true){
    $_SESSION['alert'] = "success";
  }else {
    $_SESSION['alert'] = "error";
  }
  header('location:../training?id='.$user_id);

}
 ?>

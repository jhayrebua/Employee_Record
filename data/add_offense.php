<?php
require_once('../class/c_disciplinary.php');

if(isset($_POST['offense'])){
  $offense = $_POST['offense'];
  $date = $_POST['date'];
  $id = $_POST['id'];

  $result = $disciplinary->addAction($id,$offense,$date);

  if($result === true){
    $_SESSION['alert'] = "success";
  }else {
    $_SESSION['alert'] = "error";
  }
}

 ?>


<?php
require_once('../class/c_employee.php');

if(isset($_POST['id'])){
  $id = $_POST['id'];
  $first= trim($_POST['first']);
  $middle= trim($_POST['middle']);
  $last= trim($_POST['last']);
  $section= trim($_POST['section']);
  $idnum= trim($_POST['idnum']);
  $position= trim($_POST['position']);
  $status= trim($_POST['status']);
  $date_hired= trim($_POST['date_hired']);
  $endcontract= trim($_POST['endcontract']);
  $gender= trim($_POST['gender']);
  $month= trim($_POST['month']);
  $day= trim($_POST['day']);
  $year= trim($_POST['year']);
  $contact= trim($_POST['contact']);
  $tin= trim($_POST['tin']);
  $sss= trim($_POST['sss']);
  $philhealth= trim($_POST['phil']);
  $pagibig= trim($_POST['pagibig']);
  $salary= trim($_POST['salary']);
  $remarks= trim($_POST['remarks']);
  $others= trim($_POST['others']);
  $image = $_POST['info_image'];

  if($_FILES['output1']['name']!= ''){
    $file = $_FILES['output1'];
    $file_name = $file['name'];
    $file_tmp = $file['tmp_name'];
    $file_size = $file['size'];
    $file_error = $file['error'];
    $file_ext = explode('.',$file_name);
    $file_ext = strtolower(end($file_ext));
    $allowed = array('pdf','jpg','png','jpeg');
    if(in_array($file_ext,$allowed)){
            if($file_error === 0 ){
                $file_name_new = $last."@".$idnum .".". $file_ext;
                $file_destination = '../idimg/'.$file_name_new;
                move_uploaded_file($file_tmp,$file_destination);
            }
    }//array
  }else {
    $file_name_new = $image;
  }

  $birthday = $month."-".$day."-".$year ;
  $result = $employee->editInfo($first,$middle,$last,$section,$idnum,$position,$status,$date_hired,$endcontract,$gender,$birthday,$contact,$tin,$sss,$philhealth,$pagibig,$salary,$remarks,$others,$file_name_new,$id);
  if($result === true){
    $_SESSION['alert'] = "success";
  }else {
    $_SESSION['alert'] = "error";
  }

 header('location:../list');
}
?>

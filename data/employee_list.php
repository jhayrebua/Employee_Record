<?php
require_once('../class/c_employee.php');
$result = $employee->listEmployee();
?>
<?php
$rand = rand(1,9999);
if(count($result) > 0 ){ ?>
  <table class="table table-striped table-bordered table-dark table-hover table-responsive-xl" style="width:100%" id="employee_table">
    <thead class="thead-dark">
      <tr>
        <th></th>
        <th>Name</th>
        <th>Department</th>
        <th>Status</th>
        <th>Id No.</th>
        <th>Gender</th>
      </tr>
    </thead>
    <tbody>
<?php foreach($result as $row): ?>
      <tr>
        <td><a href="info?id=<?= $row['unique_id'] ?>" title="More info"><img src="idimg/<?= $row['image']."?".$rand ?>" alt="..." style="width:5vh;height:5vh;" class="rounded-circle"></img></a></td>
        <td><?= $row['last'].", ".$row['first']." ".$row['middle'] ?></td>
        <td><?= $row['section'] ?></td>
        <td><?= $row['status']?></td>
        <td><?= $row['idnum']?></td>
        <td><?= $row['gender']?></td>
      </tr>
<?php endforeach; ?>
    </tbody>
  </table>
<?php
}else{ ?>
  <h3>NO EMPLOYEE RECORD</h3>
<?php } ?>

<script type="text/javascript">
  $("#employee_table").dataTable();
</script>

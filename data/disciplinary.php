<?php
require_once('../class/c_disciplinary.php');

if(isset($_POST['id'])){
  $id = $_POST['id'];
  $result = $disciplinary->listAction($id);

  if(count($result) > 0 ){ ?>
    <table class="table table-striped table-bordered table-hover table-responsive-xl" style="width:100%" id="training_table">
      <thead class="thead-dark">
        <tr>
          <th style="width:5%;"></th>
          <th>NATURE OF OFFENSE</th>
          <th>DATE OF ACTION</th>
        </tr>
      </thead>
      <tbody>
<?php
    foreach($result as $row):  ?>
      <tr>
        <td><button type="button" class="btn btn-danger" onClick="location.href='data/delete_offense.php?id=<?= $row['uniq'] ?>&userid=<?= $row['id'] ?>'"><span class="fas fa-trash"></span></button></td>
        <td><?= $row['offense'] ?></td>
        <td><?= $row['year'] ?></td>
      </tr>
<?php endforeach; ?>
      </tbody>
    </table>
<?php
  }else {?>
    <h3>NO DISCIPLINARY ACTION RECORD!</h3>
<?php }
  }
?>
<script type="text/javascript">
  $("#training_table").dataTable();
</script>

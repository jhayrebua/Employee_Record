<?php
require_once('../class/c_disciplinary.php');

if(isset($_GET['id'])){
  $id = $_GET['id'];
  $user_id = $_GET['userid'];
  $result = $disciplinary->deleteAction($id);

  if($result === true){
    $_SESSION['alert'] = "success";
  }else {
    $_SESSION['alert'] = "error";
  }
  header('location:../disciplinary?id='.$user_id);
}

 ?>

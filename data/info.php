<?php
require_once('../class/c_employee.php');
if(isset($_POST['id'])){
  $id = $_POST['id'];
  $result = $employee->employeeInfo($id);
  $random = rand(1,9999);
if(count($result) > 0) {
  foreach($result as $row):
 ?>
  <div class="form-group row">
    <div class="col-sm-12">
      <div class="text-center">
        <img id="output"  src="idimg/<?= $row['image']."?".$random ?>" class="img-thumbnail" alt="Your Image" style="width:240px; height:250px;"><br/>
        <input type="file" title="" id="output1" name="output1" accept="image/*" onchange="loadFile(event)"/>
      </div>
    </div>
  </div>
  <hr>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg ">First Name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" name="first" id="first" placeholder="Enter First Name" value="<?= $row['first'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label  input-lg">Middle Name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control  input-lg" id="middle" name="middle" placeholder="Enter Middle Name" value="<?= $row['middle'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label class="col-sm-2 col-form-label  input-lg">Last Name</label>
    <div class="col-sm-8">
      <input type="text" class="form-control  input-lg" id="last" name="last" placeholder="Enter Last Name" value="<?= $row['last'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label class="col-sm-2 col-form-label  input-lg">Section</label>
      <div class="col-sm-3">
        <select id="section" name="section" class="form-control  input-lg"  required>
          <option value=""></option>
          <option value="Planning">Planning Dept.</option>
          <option value="Warehouse">Warehouse Dept.</option>
          <option value="Purchasing">Purchasing Dept.</option>
          <option value="Production">Production Dept.</option>
          <option value="Accounting">Accounting Dept.</option>
          <option value="Human resources">Human Resources Dept.</option>
          <option value="Engineering">Engineering Dept.</option>
          <option value="Customer service">Customer Service Dept.</option>
        </select>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label  input-lg">I.D Number</label>
    <div class="col-sm-8">
      <input type="text" class="form-control  input-lg" id="idnum" name="idnum" placeholder="Enter ID Number" value="<?= $row['idnum'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label  input-lg">Position</label>
    <div class="col-sm-8">
      <input type="text" class="form-control  input-lg" id="position" name="position" placeholder="Enter Position" value="<?= $row['position'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label  input-lg">Employment Status</label>
    <div class="col-sm-3">
    <select id="status" name="status" class="form-control  input-lg"required>
       <option disabled></option>
       <option value="contractual">Contractual</option>
       <option value="probationary">Probationary</option>
       <option value="regular">Regular</option>
    </select>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Date Hired</label>
    <div class="col-sm-8">
       <input  type="text"  class="form-control input-lg" name="date_hired" id="date_hired" value="<?= $row['date_hired'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-3"></div>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" value="<?= $row['endcontract'] ?>" id="endcontract" name="endcontract" placeholder="End of Contract If Probationary or Contractual" required readonly>
    </div>
  </div>
  <hr>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Gender</label>
      <div class="col-sm-8">
        <select id="gender" name="gender" class="form-control input-lg" name="gender" style="width:200px;" required>
          <option value="" disabled selected >Select gender</option>
          <option value="Male">Male</option>
          <option value="Female">Female</option>
        </select>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Birth Date</label>
    <div class="col-sm-4">
      <select id="month" name="month" class="form-control input-lg"  required>
        <option value="" disabled selected >Month</option>
        <option value="January">January</option>
        <option value="February">February</option>
        <option value="March">March</option>
        <option value="April">April</option>
        <option value="May">May</option>
        <option value="August">August</option>
        <option value="September">September</option>
        <option value="October">October</option>
        <option value="November">November</option>
        <option value="December">December</option>
      </select>
    </div>
    <div class="col-sm-2">
      <?php
        echo ("<select id='day' name='day' class='form-control input-lg' required>");
          echo ("<option value='' disabled selected>Day</option>");
                for($a=1;$a<=31;$a++){
                  echo ("<option value='".$a."'>".$a."</option>");
                 }echo "</select>";?>
    </div>
    <div class="col-sm-2">
      <?php
        echo ("<select id='year' name='year' class='form-control input-lg'  required>");
          echo ("<option value='' disabled selected>Year</option>");
            for($a=2017;$a>=1950;$a--){
              echo ("<option value='".$a."'>".$a."</option>");
             }echo ("</select>"); ?>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Contact Number</label>
      <div class="col-sm-8">
        <input type="text" class="form-control input-lg" id="contact" name="contact" placeholder="Enter Contact No." value="<?= $row['contact'] ?>" required>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">TIN NO.</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" id="tin" name="tin" value="<?= $row['tin'] ?>" placeholder="Enter TIN No." required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">SSS NO.</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" id="sss" name="sss"placeholder="Enter SSS No." value="<?= $row['sss'] ?>" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">PHILHEALTH</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" id="phil" name="phil" value="<?= $row['philhealth'] ?>" placeholder="Enter Philhealth No." required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">PAG IBIG NO.</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" id="pagibig" name="pagibig" value="<?= $row['pagibig'] ?>" placeholder="Enter Pagibig No." required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Salary</label>
    <div class="col-sm-8">
      <input type="text" class="form-control input-lg" id="salary" name="salary" value="<?= $row['salary'] ?>" placeholder="Enter Salary" required>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg">Remarks</label>
      <div class="col-sm-8">
        <select id="remarks" name="remarks" class="form-control input-lg" name="remarks" style="width:200px;" required>
          <option value=""></option>
          <option value="active">Active</option>
          <option value="notactive">Not Active</option>
          <option value="others">Others</option>
        </select>
      </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-1"></div>
    <label  class="col-sm-2 col-form-label input-lg"></label>
    <div class="col-sm-8">
        <input type="text" class="form-control input-lg" id="others" name="others" value="<?= $row['others'] ?>" placeholder="Remarks : (if OTHERS)" readonly>
    </div>
  </div>
  <div class="form-group row">
    <div class="col-sm-11">
      <button style="float:right;" type="submit" class="btn btn-primary input-lg">Submit</button>
    </div>
  </div>
  <input  class="form-control input-lg" id="select" value="<?= $row['section'] ?>" type="hidden">
  <input  class="form-control input-lg" id="birthday2" value="<?=  $row['birthday'] ;?>" type="hidden">
  <input  class="form-control input-lg" id="gender2" value="<?=  $row['gender'] ;?>" type="hidden">
  <input  class="form-control input-lg" id="remarks2" value="<?=  $row['remarks'] ;?>" type="hidden">
  <input  class="form-control input-lg" id="status2" value="<?=  $row['status'] ;?>" type="hidden">
  <input  class="form-control input-lg" id="end" value="<?=  $row['endcontract'] ;?>" type="hidden">
  <input  class="form-control input-lg" name="id"  id="id"   value="<?=  $row['unique_id'] ?>" type="hidden">
  <input type="hidden" name="info_image" id="info_image" value="<?= $row['image'] ?>">
<?php
  endforeach;
  }
} ?>

<script type="text/javascript">
function edithr(){
  var setter = $("#select").val().trim();
    $('#section').val(setter).change();
  var gen = $("#gender2").val().trim();
    $('#gender').val(gen).change();
  var birthday = $("#birthday2").val().trim();
  var splitter = birthday.split('-');
    $('#month').val(splitter[0]).change();
    $('#day').val(splitter[1]).change();
    $('#year').val(splitter[2]).change();
  var status = $("#status2").val().trim();
    $('#status').val(status).change();
  var remarks = $("#remarks2").val().trim();
    $('#remarks').val(remarks).change();
  var end = $("#end").val().trim();
    $('#endcontract').val(end);
};
$( "#date_hired" ).datepicker({ dateFormat: 'yy-mm-dd'});

  $("#date_hired,#status").change(function (e){
    e.preventDefault();
     var val  = $("#date_hired").val();
     // alert(val);
     var splitter = val.split('-');
     var year = splitter[0];
     var month = splitter[1];
     var day = splitter[2];
     var counter = 0;
     year = parseInt(year);
     var status = $("#status").val();
     if(status=="contractual"){
      month = parseInt(month) + 5;
      if(month>12){
        while(month>12){
          month--;
          counter++;
        }
         var year2 = year + 1;
         var month1 = counter;
         $( "#endcontract" ).val(year2+"-"+month1+"-"+day);
       }else
         $( "#endcontract" ).val(year+"-"+month+"-"+day);
     }else if(status=="probationary"){
        month = parseInt(month) + 6;
        if(month>12){
          while(month>12){
            month--;
            counter++;
          }
          var year2 = year + 1;
          var month1 = counter;
          $( "#endcontract" ).val(year2+"-"+month1+"-"+day);
       }else
            $( "#endcontract" ).val(year+"-"+month+"-"+day);
      }else
        $( "#endcontract" ).val("N/A");
  }); //date hired
  // alert("");

edithr();
</script>
